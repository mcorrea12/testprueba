import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;


public class testCalculadora {
    //TEST SUMA
    @Test
    public void testSuma(){
	int resultado = Calculadora.Suma(3,0);
	int esperado = 3;
	assertEquals(esperado,resultado);
}   
  @Test
    public void testSumanegativo(){
	int resultado = Calculadora.Suma(-3,-2);
	int esperado = -5;
	assertEquals(esperado,resultado);
} 
    @Test
    public void testSumanegativoPositivo(){
	int resultado = Calculadora.Suma(3,-2);
	int esperado = 1;
	assertEquals(esperado,resultado);
    }
    //TEST RESTA
    @Test
    public void testResta(){
    int resultado = Calculadora.Resta(3,1);
    int esperado = 2;
    assertEquals(esperado,resultado);
    }
    @Test
    public void testRestaNegativos(){
    int resultado = Calculadora.Resta(-2,-1);
    int esperado = -1;
    assertEquals(esperado,resultado);
    } 
    @Test
    public void testRestaNegativoPositivo(){
    int resultado = Calculadora.Resta(-2,1);
    int esperado = -3;
    assertEquals(esperado,resultado);
    } 
    //TEST MULTIPLICACION
    @Test
    public void testMultiplicar(){
    int resultado = Calculadora.Multiplicar(4,2);
    int esperado = 8;
    assertEquals(esperado,resultado);
    }
    @Test
    public void testMultiplicarNegativos(){
    int resultado = Calculadora.Multiplicar(-4,-2);
    int esperado = 8;
    assertEquals(esperado,resultado);
    }
     @Test
    public void testMultiplicarPorCero(){
    int resultado = Calculadora.Multiplicar(4,0);
    int esperado = 0;
    assertEquals(esperado,resultado);
    }
    
    //TEST DIVISION
    @Test
    public void testDividir(){
    int resultado = Calculadora.Dividir(4,2);
    int esperado = 2;
    assertEquals(esperado,resultado);
    }
    @Test
    public void testDividirNegativos(){
    int resultado = Calculadora.Dividir(-4,-2);
    int esperado = 2;
    assertEquals(esperado,resultado);
    }
     @Test
    public void testDividirNegativosPositivo(){
    int resultado = Calculadora.Dividir(4,-2);
    int esperado = -2;
    assertEquals(esperado,resultado);
    }
   
   
}
